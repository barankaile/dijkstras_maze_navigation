from p1_support import load_level, show_level, save_level_costs
from math import inf, sqrt
from heapq import heappop, heappush

#Algorithm inspiration taken from Introduction to A* article by Amit Patel
def dijkstras_shortest_path(initial_position, destination, graph, adj):
    """ Searches for a minimal cost path through a graph using Dijkstra's algorithm.

    Args:
        initial_position: The initial cell from which the path extends.
        destination: The end location for the path.
        graph: A loaded level, containing walls, spaces, and waypoints.
        adj: An adjacency function returning cells adjacent to a given cell as well as their respective edge costs.

    Returns:
        If a path exits, return a list containing all cells from initial_position to destination.
        Otherwise, return None.

    """
    # Initialize empty queue
    queue = []
    # Initialize dist dictionary
    dist = {}
    # Initialize prev dictionary
    prev = {}
    # Retrieve list of neighbors from nav edges function and put it in list navEdges

    # Set waypoint default distance of source position
    dist[initial_position] = 0
    prev[initial_position] = None
    # Put initial node into queue
    queue.append((0, initial_position))

    # Main loop
    while len(queue) != 0:  # While there are nodes to process
        # Pop off next node to process
        new_target = heappop(queue)

        if new_target[1] == destination:
            break

        # Process all neighbors from navEdges
        navEdges = adj(graph, new_target[1])

        for neighbor in navEdges:
            # Calculate new distance to determine if distance is shorter
            alt = dist[new_target[1]] + neighbor[0]
            # Checking if the distance is less than what is currently stored
            if neighbor[1] not in dist or alt < dist[neighbor[1]]:
                # If not then assign it the new value and keep track of running distance
                dist[neighbor[1]] = alt
                heappush(queue, neighbor)
                # Set path from where it came from
                prev[neighbor[1]] = new_target[1]

    path = []
    if destination not in prev:
        return None
    path.append(destination)

    while prev[destination] != None:
        path.append(prev[destination])
        destination = prev[destination]

    return path

def dijkstras_shortest_path_to_all(initial_position, graph, adj):
    """ Calculates the minimum cost to every reachable cell in a graph from the initial_position.

    Args:
        initial_position: The initial cell from which the path extends.
        graph: A loaded level, containing walls, spaces, and waypoints.
        adj: An adjacency function returning cells adjacent to a given cell as well as their respective edge costs.

    Returns:
        A dictionary, mapping destination cells to the cost of a path from the initial_position.
    """
    # Initialize empty queue
    queue = []
    # Initialize dist dictionary
    dist = {}
    # Initialize prev dictionary
    prev = {}
    # Retrieve list of neighbors from nav edges function and put it in list navEdges

    # Set waypoint default distance of source position
    dist[initial_position] = 0
    prev[initial_position] = None
    # Put initial node into queue
    queue.append((0, initial_position))

    # Main loop
    while len(queue) != 0:  # While there are nodes to process
        # Pop off next node to process
        new_target = heappop(queue)

        # Process all neighbors from navEdges
        navEdges = adj(graph, new_target[1])

        for neighbor in navEdges:
            # Calculate new distance to determine if distance is shorter
            alt = dist[new_target[1]] + neighbor[0]
            # Checking if the distance is less than what is currently stored
            if neighbor[1] not in dist or alt < dist[neighbor[1]]:
                # If not then assign it the new value and keep track of running distance
                dist[neighbor[1]] = alt
                heappush(queue, neighbor)
                # Set path from where it came from
                prev[neighbor[1]] = new_target[1]

    #path = []
    #path.append(destination)
    #while prev[destination] != None:
    #    path.append(prev[destination])
    #    destination = prev[destination]

    return dist


def navigation_edges(level, cell):
    """ Provides a list of adjacent cells and their respective costs from the given cell.

    Args:
        level: A loaded level, containing walls, spaces, and waypoints.
        cell: A target location.

    Returns:
        A list of tuples containing an adjacent cell's coordinates and the cost of the edge joining it and the
        originating cell.

        E.g. from (0,0):
            [((0,1), 1),
             ((1,0), 1),
             ((1,1), 1.4142135623730951),
             ... ]
    """
    tup_list = []
    # print(cell[1])
    for i in range(-1, 2):
        for j in range(-1, 2):
            # print(i,j)
            # if (i is not 0) and (j is not 0):
            cell_1 = cell[0] + j
            cell_2 = cell[1] + i
            new_cell = (cell_1, cell_2)
            if new_cell not in level['walls']:
                tup_list.append((move_cost(cell, new_cell, level), new_cell))

    #tup_list.remove((cell, 0))
    return tup_list

def move_cost(start, end, level):  # Calculates the move cost from one square to another, assumes the squares are next to each other and that neither square is a wall
    di = .5*sqrt(2)

    if start not in level['spaces']:
        cost_start = 1
    else:
        cost_start = level['spaces'][start]
    if end not in level['spaces']:
        cost_end = 1
    else:
        cost_end = level['spaces'][end]

    if start[0] != end[0] and start[1] != end[1]:
        return di*cost_start + di*cost_end
    #distance = sqrt((end[0] - start[0]) ** 2 + (end[1] - start[1]) ** 2)
    else:
        return .5*cost_start + .5*cost_end
    #return cost_start * (distance / 2) + cost_end * (distance / 2)

def test_route(filename, src_waypoint, dst_waypoint):
    """ Loads a level, searches for a path between the given waypoints, and displays the result.

    Args:
        filename: The name of the text file containing the level.
        src_waypoint: The character associated with the initial waypoint.
        dst_waypoint: The character associated with the destination waypoint.

    """

    # Load and display the level.
    level = load_level(filename)
    show_level(level)

    # Retrieve the source and destination coordinates from the level.
    src = level['waypoints'][src_waypoint]
    dst = level['waypoints'][dst_waypoint]

    # Search for and display the path from src to dst.
    path = dijkstras_shortest_path(src, dst, level, navigation_edges)
    if path:
        show_level(level, path)
    else:
        print("No path possible!")


def cost_to_all_cells(filename, src_waypoint, output_filename):
    """ Loads a level, calculates the cost to all reachable cells from 
    src_waypoint, then saves the result in a csv file with name output_filename.

    Args:
        filename: The name of the text file containing the level.
        src_waypoint: The character associated with the initial waypoint.
        output_filename: The filename for the output csv file.

    """
    
    # Load and display the level.
    level = load_level(filename)
    show_level(level)

    # Retrieve the source coordinates from the level.
    src = level['waypoints'][src_waypoint]
    
    # Calculate the cost to all reachable cells from src and save to a csv file.
    costs_to_all_cells = dijkstras_shortest_path_to_all(src, level, navigation_edges)
    save_level_costs(level, costs_to_all_cells, output_filename)


if __name__ == '__main__':
    filename, src_waypoint, dst_waypoint = 'test_maze.txt', 'a','d'

    level = load_level(filename)

    # Use this function call to find the route between two waypoints.
    test_route(filename, src_waypoint, dst_waypoint)

    # Use this function to calculate the cost to all reachable cells from an origin point.
    cost_to_all_cells(filename, src_waypoint, 'my_maze_costs.csv')
